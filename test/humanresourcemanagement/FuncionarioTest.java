
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresourcemanagement;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucaswilliam
 */
public class FuncionarioTest {

    public FuncionarioTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAtivo() {
        Funcionario funcionario1 = new Funcionario("Tadeu gay", "Administração", 8000, "12-12-1914", null);

        List<Funcionario> funcionarios = new ArrayList<>();
        funcionarios.add(funcionario1);

        String status = funcionario1.getSituacao();

        assertEquals(status, "Ativo");
//        assertEquals(status, "Demitido");
    }

    @Test
    public void testDemitido() {
        Funcionario funcionario1 = new Funcionario("Tadeu gay", "Administração", 8000, "12-12-1914", "14-12-1926");

        String status = funcionario1.getSituacao();

//        assertEquals(status, "Ativo");
        assertEquals(status, "Demitido");
    }

    @Test
    public void testGetSalarioBruto() {
        Funcionario funcionario1 = new Funcionario("Tadeu gay", "Administração", 1900, "12-12-1914", "14-12-1926");

        double salario = funcionario1.calcularSalarioLiquido(funcionario1.getSalarioBruto());

        assertEquals(salario, 1900, 0);
    }

    @Test
    public void testcalcularSalarioLiquido() {
        Funcionario funcionario1 = new Funcionario("Tadeu gay", "Administração", 8000, "12-12-1914", "14-12-1926");

        double salario = funcionario1.calcularSalarioLiquido(funcionario1.getSalarioBruto());
//        System.out.println(salario);
        assertEquals(salario, 5800, 0);
    }
}
