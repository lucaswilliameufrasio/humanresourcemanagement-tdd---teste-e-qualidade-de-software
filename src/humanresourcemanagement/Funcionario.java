/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package humanresourcemanagement;

/**
 *
 * @author lucaswilliam
 */
public class Funcionario {

    private String nome;
    private String setor;
    private double salarioBruto;
    private String dataAdmissao;
    private String dataDemissao;

    public Funcionario(String nome, String setor, double salarioBruto, String dataAdmissao, String dataDemissao) {
        this.nome = nome;
        this.setor = setor;
        this.salarioBruto = salarioBruto;
        this.dataAdmissao = dataAdmissao;
        this.dataDemissao = dataDemissao;
    }

    public String getSituacao() {
        if (getDataDemissao() != null) {
            return "Demitido";
        }
        return "Ativo";
    }

    public double calcularSalarioLiquido(double salario) {
        if (salario <= 1900) {
            return salario;
        } else if (salario > 1900 && salario <= 2800) {
            return ((salario * 92.5) / 100);
        } else if (salario > 2800 && salario <= 3750) {
            return ((salario * 85) / 100);

        } else if (salario > 3750 && salario <= 4660) {
            return ((salario * 77.5) / 100);

        } else if (salario > 4660) {
            return ((salario * 72.5) / 100);
        }
        return 0;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public double getSalarioBruto() {
        return salarioBruto;
    }

    public void setSalarioBruto(double salarioBruto) {
        this.salarioBruto = salarioBruto;
    }

    public String getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(String dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getDataDemissao() {
        return dataDemissao;
    }

    public void setDataDemissao(String dataDemissao) {
        this.dataDemissao = dataDemissao;
    }

}
